const { response } = require('express');
const { validationResult } = require('express-validator');
const Usuario = require('../models/Usuario')
const bcrypt = require('bcryptjs')
const {generarJWT} = require('../helpers/jwt')

const crearUsuario = async (req, res = response) => {

    const {email,name, password} = req.body;

    try{



        // Verificar el email para
        const usuario = await Usuario.findOne({email: email});

        if(usuario){
            return res.status(400).json({
                ok:false,
                msg:'El usuario ya existe'
            });
        }

        //Crear usuario con el modelo
        const dbUser = new Usuario(req.body);

        //Hashear la contrasenia
        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync(password,salt);


        //Generar el JWT
        const token = await generarJWT(dbUser.id, name)

        //Crear usuario de DB
        await dbUser.save();

        //Generar respuesta exitosa
        return res.status(200).json({
            ok:true,
            id:dbUser.id,
            name,
            token
        });


    }catch(error){
        console.log(error);
        return res.status(500).json({
            ok:false,
            msg:'Algo salio mal'
        });
    }


    

   
}


const loginUsuario = async(req, res = response) => {

    const {email, password} = req.body;

    try {

        const dbUser = await Usuario.findOne({email});

        if(!dbUser) {
            return res.status(400).json({
                ok: false,
                msg:'Credenciales no validas'
            });
        }

        //Confirmar si el password hace match
        const validPassword = bcrypt.compareSync(password, dbUser.password);

        if(!validPassword) {
            return res.status(400).json({
                ok: false,
                msg:'Credenciales no validas'
            });
        }

        // Generar el JWT
        const token = await generarJWT(dbUser.id, dbUser.name)

        //Respuesta del servicio
        return res.json({
            ok: true,
            id: dbUser.id,
            name: dbUser.name,
            token
        })



        
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg:'Algo salio mal'
        })
    }

   
}

const validarTokenSesion = async (req, res = response) => {

    const {id, name} = req;

    // Generar el JWT
    const token = await generarJWT(id, name)

    return res.json({
        ok:true,
        id, 
        name,
        token
    });
}




module.exports = {
    crearUsuario,
    loginUsuario,
    validarTokenSesion
}